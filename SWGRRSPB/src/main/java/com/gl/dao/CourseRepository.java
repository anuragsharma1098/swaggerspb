package com.gl.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.model.Course;

public interface CourseRepository extends JpaRepository<Course, Integer>{

}
