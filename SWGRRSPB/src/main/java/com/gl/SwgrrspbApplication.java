package com.gl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwgrrspbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwgrrspbApplication.class, args);
	}

}
